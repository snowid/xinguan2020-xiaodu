发起单位：[数字天堂（北京）网络技术有限公司](http://www.dcloud.io/)

全体贡献者名单如下：

  项目负责人：[林举](http://baike.baidu.com/item/%E6%9E%97%E4%B8%BE/22887913?fr=aladdin)



        项目UI设计师：沈淳

        程序开发：林举、王运、董连朔、刘瑶瑶、曹钧豪、刘德鑫

        逻辑整理：刘小美


联系邮箱：mail@0577app.com

## 部署声明

本项目为采用由中国Dcloud公司研发的Hbuilder-X编辑器和uniapp框架编译的HybridApp+h5+微信小程序
本项目后端采用由中国Dcloud公司研发的unicloud（serverless技术，底层语言：nodejs+Nsql数据库；可抗百万并发）

##实施：

1.同步上传并部署本项目cloudfunctions-aliyun文件夹下的所有云函数；

2.创建云数据库，数据表名：location、user、disinfect

3.配置云服务空间；打开根目录common文件夹的app.js文件修改里面的：provider、spaceId、clientSecret

如：
const myCloud = uniCloud.init({
	provider: 'aliyun',
	spaceId: 'xxxxxxxx-xxx-xxx-xxx-xxxxxxxx',
	clientSecret: 'xxxxxtrdwysuiodccyg=='
});


3.执行云函数creat_admin创建默认账号；账号：admin 密码：123456

4.有任何问题请加qq群群号:120575618，请备注部门单位名称加群，如：温州市xx局、温州市xx医院等。


![Image text](https://images.gitee.com/uploads/images/2020/0206/222009_8dd6931d_5163472.png)
![Image text](https://images.gitee.com/uploads/images/2020/0206/222048_fdabd528_5163472.png)

## 需求说明
角色：	1.管理员 2.工作人员
管理员为消毒场所的管理人员如医院的物业，使用本应用可
1.管理账号-包括管理员和工作人员账号
2.设置消毒位置
3.检查所有工作人员的消毒情况-包括消毒：时间、照片证明、地址、经纬度信息、工作者姓名

工作人员为消毒工作人员
通过本应用上报自己的消毒工作，页可查看自己的消毒历史记录
![Image text](https://images.gitee.com/uploads/images/2020/0206/222130_a6dc4ea8_5163472.png)
![Image text](https://images.gitee.com/uploads/images/2020/0206/222157_1db00777_5163472.jpeg)